#pragma once
#include"Vec2.h"
#include"RectF.h"
#include"Keyboard.h"
#include"FrameTimer.h"
#include"Colors.h"
#include"Graphics.h"
class Player
{
public:

	Player player(const Vec2& pos_in, float halfWidth_in, float halfheight_in);
	void  Draw(const Graphics& gfx);
	bool  DoWallCoalision(const RectF& walls);
	//bool  DoBallCoalision(const Ball& ball);

	

private: 

	RectF rect;
	Color color = Colors::White;
	float halfWidth;
	float halfHeight;
	Vec2 pos;
	float speed = 300.0f;




};