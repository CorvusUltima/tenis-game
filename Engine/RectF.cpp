#include "RectF.h"

RectF::RectF(float left_in, float right_in, float top_in, float bottom_in)
	:
	left(left_in),
	right(right_in),
	top(top_in),
	bottom(bottom_in)
{
}

RectF::RectF(Vec2& topLeft, Vec2& bottomRight)
	:
	RectF( topLeft.x,  bottomRight.x,  topLeft.y, bottomRight.y)
{
}

RectF::RectF(Vec2 topleft, float width, float height)
	:
	RectF(topleft,topleft+Vec2(width,height))
{
}

bool RectF::IsOverLappingWith(const RectF& other)
{
	return right > other.left && left<other.right&&
	bottom>other.top && top < other.bottom;
}


