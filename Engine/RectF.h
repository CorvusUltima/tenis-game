#pragma once
#include"Vec2.h"
class RectF
{
public:
	RectF() = default;
	RectF (float left_in, float right_in, float top_in, float bottom_in);
	RectF (Vec2& topLeft, Vec2& bottomRight);
	RectF(Vec2 topleft, float width, float height);
	bool IsOverLappingWith(const RectF& other);

	




public:
	float left;
	float right;
	float top;
	float bottom;


};